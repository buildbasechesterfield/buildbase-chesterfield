For all your building materials, timber & DIY needs. 
When you trade with Buildbase, you’ll get everything you need in one place. We take pride in serving the local tradespeople and our communities, working with our customers to get the job done, so you can rely on us to give a brilliant service.

Address: Newbold Rd, Chesterfield S41 7PB, United Kingdom

Phone: +44 1246 203201

Website: [https://www.buildbase.co.uk/storefinder/store/Chesterfield-1308](https://www.buildbase.co.uk/storefinder/store/Chesterfield-1308)
